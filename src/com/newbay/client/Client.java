package com.newbay.client;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Client {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager
					.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
		}

		final ConnectionManager conn = new ConnectionManager();
		try {
			conn.connection();
		} catch (IOException e) {
			e.printStackTrace();
		}

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				ClientFrame clientFrame = new ClientFrame("NewBay.com", conn);

				clientFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				clientFrame.pack();
				clientFrame.setSize(500, 250);
				clientFrame.setVisible(true);
			}
		});

	}
}