package com.newbay.client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.newbay.server.ApplicationServer;

public class ClientFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private static String labelPrefix = "Counter: ";
	private static JLabel label;

	private final ConnectionManager conn;

	public ClientFrame(String title, ConnectionManager conn) {
		super(title);
		this.conn = conn;

		MyEventListener listener = new MyEventListener() {

			@Override
			public void onMessage(final MyEvent event) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						label.setVisible(true);
						label.setText(labelPrefix + event.getString());
					}
				});

			}
		};
		conn.setListener(listener);

		Component contents = createComponents();
		add(contents, BorderLayout.CENTER);

		addWindowListener(new MyWindowAdapter());

		final ConnectionManager conn2 = conn;
		new Thread() {
			public void run() {
				try {
					conn2.makeMeListeningForServerMessages();
				} catch (IOException e) {

				}
			};
		}.start();

	}

	public Component createComponents() {
		label = new JLabel(labelPrefix + "0    ");
		label.setVisible(false);

		JButton buttonAdd = new JButton("+");
		buttonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					conn.addCounter();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		label.setLabelFor(buttonAdd);

		JButton buttonRemove = new JButton("-");
		buttonRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					conn.removeCounter();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		label.setLabelFor(buttonRemove);

		JPanel pane = new JPanel();
		pane.setBorder(BorderFactory.createEmptyBorder(30, // top
				30, // left
				10, // bottom
				30) // right
		);
		pane.setLayout(new GridLayout(0, 1));
		pane.add(buttonAdd);
		pane.add(buttonRemove);
		pane.add(label);

		return pane;
	}

	private final class MyWindowAdapter extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent e) {
			try {
//				ApplicationServer.saveCounter();
				conn.closeConnections();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		@Override
		public void windowClosed(WindowEvent e) {
			try {
//				ApplicationServer.saveCounter();
				conn.closeConnections();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

}
