package com.newbay.client;

public class MyEvent {

	private final String string;

	public MyEvent(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}