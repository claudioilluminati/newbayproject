package com.newbay.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.newbay.server.ApplicationServer;
import com.newbay.server.Server;

public class ConnectionManager {

	private Socket echoSocket = null;
	private PrintWriter out = null;
	private BufferedReader in = null;

	private MyEventListener listener;

	public ConnectionManager() {
	}

	public void setListener(final MyEventListener listener) {
		this.listener = listener;
	}

	public void connection() throws IOException {
		try {
			echoSocket = new Socket("localhost", 4444);
			out = new PrintWriter(echoSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					echoSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about the server");
			System.exit(1);
		} catch (IOException e) {
			System.err
					.println("Couldn't get I/O for the connection to the server");
			System.exit(1);
		}

	}

	public void makeMeListeningForServerMessages() throws IOException {
		String userInput;
		if (echoSocket.isConnected()) {
			while ((userInput = in.readLine()) != null) {
				listener.onMessage(new MyEvent(userInput));
			}
		}
	}

	public void addCounter() throws IOException {
		out.println("+");
	}

	public void removeCounter() throws IOException {
		out.println("-");
	}

	public void closeConnections() throws IOException {
		if (out != null)
			out.close();
		if (in!=null)
			in.close();
		ApplicationServer.deregisterClient(echoSocket);
		if (echoSocket!= null)
			echoSocket.close();
	}

}
