package com.newbay.server;

public class Server {

	public static void main(String[] args) {

		int port = 4444;
		int maxConnections = 10;

		ApplicationServer myServer = new ApplicationServer(port, maxConnections);
		myServer.start();

	}

}
