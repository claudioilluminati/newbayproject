package com.newbay.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class ApplicationServer {

	public static int counter = 0;
	public static Vector<Socket> listOfClients = new Vector<Socket>();

	private final int port;
	private final int maxConnections;


	public ApplicationServer(int port, int maxConnections) {
		this.port = port;
		this.maxConnections = maxConnections;
	}

	public void start() {
		int i = 0;

		try {
			ServerSocket listener = new ServerSocket(port);

			while ((i++ < maxConnections) || (maxConnections == 0)) {
				Socket socket = listener.accept();
				listOfClients.add(socket);

				ApplicationServer.counter = restoreCounter();
				ConnectionServerThread conn_c = new ConnectionServerThread(socket);
				Thread t = new Thread(conn_c);
				t.start();
			}
		} catch (IOException ioe) {
			System.out.println("IOException on socket listen: " + ioe);
			ioe.printStackTrace();
		} finally {
			saveCounter();
		}
	}

	public static void saveCounter() {
		try {
			System.out.println("saveCounter()");
			File f = new File("counter.ser");
			FileOutputStream fo = new FileOutputStream(f);
			ObjectOutputStream objOut = new ObjectOutputStream(fo);
			objOut.writeInt(ApplicationServer.counter);
			objOut.flush();
			objOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int restoreCounter() {
		int restoredCounter = 0;

		File f = new File("counter.ser");
		if (f.exists()) {
			try {
				FileInputStream fi = new FileInputStream(f);
				ObjectInputStream objIn = new ObjectInputStream(fi);
				restoredCounter = objIn.readInt();
				objIn.close();

				return restoredCounter;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return restoredCounter;
	}
	
	public static void deregisterClient (Socket s) {
		listOfClients.removeElement(s);
	}

}
