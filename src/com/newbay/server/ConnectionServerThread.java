package com.newbay.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ConnectionServerThread extends Thread {

	private Socket socket;

	public ConnectionServerThread(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			// Get input from the client
			InputStreamReader in = new InputStreamReader(
					socket.getInputStream());
			BufferedReader bin = new BufferedReader(in);
			// PrintStream out = new PrintStream(server.getOutputStream());

			String input;
			while ((input = bin.readLine()) != null) {
				if ("+".equals(input)) {
					ApplicationServer.counter++;
				} else if ("-".equals(input)) {
					ApplicationServer.counter--;
				}
				// out.println(counter);
				for (int i = 0; i < ApplicationServer.listOfClients.size(); i++) {
					Socket singleClient = ApplicationServer.listOfClients
							.get(i);
					PrintStream outForSingleClient = new PrintStream(
							singleClient.getOutputStream());
					outForSingleClient.println(ApplicationServer.counter);
				}
				
				ApplicationServer.saveCounter();
			}

		} catch (IOException ioe) {
			System.out.println("IOException on socket listen: " + ioe);
			ioe.printStackTrace();
		}
	}
}